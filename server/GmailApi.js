var axios = require('axios');
var qs = require('qs');

class GmailApi{
    accessToken = "";
    constructor() {
        this.accessToken = this.getAcceToken();
      }
    getAcceToken = async () => {

var data = qs.stringify({
    'client_id': '417434049212-2c96b4rc9ccqfgsnd5h6p0t0oelac1ov.apps.googleusercontent.com',
    'client_secret': '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',
    'refresh_token': '1//0gLb_hyITq6j_CgYIARAAGBASNwF-L9IrgeexrCBR9YxT0CD6i4J2Gwny0sNr0BGFvnpjFs_pPg6ylvqTyPds7-CWX3Lk9RGVUJ8',
    'grant_type': 'refresh_token' 
  });
  var config = {
    method: 'post',
    url: 'https://accounts.google.com/o/oauth2/token',
    headers: { 
      'Content-Type': 'application/x-www-form-urlencoded', 
      'Cookie': '__Host-GAPS=1:K_gIrc9ndPkrwE4mt7_LAyFfOuDZsw:sbRieeK0fCBkP9lw'
    },
    data : data
  };
  let accessToken = "";


  await axios(config)
    .then(async function (response) {
      accessToken = await response.data.access_token;

      console.log("Access Token " + accessToken);
    })
    .catch(function (error) {
      console.log(error);
    });

  return accessToken;
};

searchGmail = async (searchItem) => {
  var config1 = {
    method: "get",
    url:
      "https://www.googleapis.com/gmail/v1/users/me/messages?q=" + searchItem,
    headers: {
      Authorization: `Bearer ${await this.accessToken} `,
    },
  };
  var threadId = "";

  await axios(config1)
    .then(async function (response) {
      threadId = await response.data["messages"][0].id;

      console.log("ThreadId = " + threadId);
    })
    .catch(function (error) {
      console.log(error);
    });

  return threadId;
};

readGmailContent = async (messageId) => {
  var config = {
    method: "get",
    url: `https://gmail.googleapis.com/gmail/v1/users/me/messages/${messageId}`,
    headers: {
      Authorization: `Bearer ${await this.accessToken}`,
    },
  };

  var data = {};

  await axios(config)
    .then(async function (response) {
      data = await response.data;
    })
    .catch(function (error) {
      console.log(error);
    });

  return data;
};

readInboxContent = async (searchText) => {
  const threadId = await this.searchGmail(searchText);
  const message = await this.readGmailContent(threadId);

  const encodedMessage = await message.payload["parts"][0].body.data;

  const decodedStr = Buffer.from(encodedMessage, "base64").toString("ascii");

  console.log(decodedStr);

  return decodedStr;
};
}


module.exports = new GmailApi();