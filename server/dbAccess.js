const mysqlx = require("@mysql/xdevapi");

class dbAccess {

getAllTable = (table)  => {
    mysqlx.getSession('mysqlx://root:lovecats4life@localhost:33060/hotel_inventory')
        .then(session => {
            return session.sql(`SELECT * FROM ${table}`).execute()
            .then(res => {
                console.log(res.fetchAll());
            })
    })
    .catch(err => {
      console.log(err);
    })
  };
  
getRooms(type) {
    mysqlx.getSession('mysqlx://root:lovecats4life@localhost:33060/hotel_inventory')
        .then(session => {
            return session.sql(`SELECT * FROM rooms WHERE ${type}`).execute()
            .then(res => {
                console.log(res.fetchAll());
            })
    })
    .catch(err => {
      console.log(err);
    })
  }
  
getAvailable(name, table) {
    mysqlx.getSession('mysqlx://root:lovecats4life@localhost:33060/hotel_inventory')
        .then(session => {
            return session.sql(`SELECT available FROM ${table} WHERE name=${name}`).execute()
            .then(res => {
                console.log(res.fetchAll());
            })
    })
    .catch(err => {
      console.log(err);
    })
  }
  
getDefault(name, table) {
    mysqlx.getSession('mysqlx://root:lovecats4life@localhost:33060/hotel_inventory')
        .then(session => {
            return session.sql(`SELECT default FROM ${table} WHERE name=${name}`).execute()
            .then(res => {
                console.log(res.fetchAll());
            })
    })
    .catch(err => {
      console.log(err);
    })
  }
  
updateAvailable(name, qty, table) {
    mysqlx.getSession('mysqlx://root:lovecats4life@localhost:33060/hotel_inventory')
        .then(session => {
            return session.sql(`UPDATE ${table} SET available=${qty} WHERE name="${name}"`).execute()
            .then(res => {
                console.log(res.fetchAll());
            })
    })
    .catch(err => {
      console.log(err);
    })
  }
  
updateDefault(name, qty, table) {
    mysqlx.getSession('mysqlx://root:lovecats4life@localhost:33060/hotel_inventory')
        .then(session => {
            return session.sql(`UPDATE ${table} SET default=${qty} WHERE name="${name}"`).execute()
            .then(res => {
                console.log(res.fetchAll());
            })
    })
    .catch(err => {
      console.log(err);
    })
  }


getItemNames(table) {
    mysqlx.getSession('mysqlx://root:lovecats4life@localhost:33060/hotel_inventory')
        .then(session => {
            return session.sql(`SELECT name from ${table}`).execute()
            .then(res => {
                console.log(res.fetchAll());
            })
    })
    .catch(err => {
      console.log(err);
    })
    
}
}

module.exports = new dbAccess();
